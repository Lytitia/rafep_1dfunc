# Makefile for RAFEP
# Letitia 2019.06.13

FC = gfortran
FFLAGS = -O2

# Define targets to be compiled
target1 = rafep.exe
target2 = fep.exe
target3 = fakeje.exe
objects1 = stats.o modules.o rafep.o
objects2 = stats.o modules.o fep.o
objects3 = stats.o modules.o fakeje.o

# Define compiling rules (all objects and final executable)
all: $(target1) $(target2) $(target3)

$(target1): $(objects1)
	$(FC) $(FFLAGS) $(objects1) -o $@

$(target2): $(objects2)
	$(FC) $(FFLAGS) $(objects2) -o $@

$(target3): $(objects3)
	$(FC) $(FFLAGS) $(objects3) -o $@

%.o: %.f90
	$(FC) $(FFLAGS) -c $<

# For clean up
clean:
	rm -f $(target1) $(target2) $(target3) *.o *.mod

.PHONY: clean all

