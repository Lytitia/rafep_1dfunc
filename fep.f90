PROGRAM FEP
USE inparas
USE potential1d
USE montecarlo
USE stats
  IMPLICIT NONE
  INTEGER*4 :: i, j
  REAL*8 :: xinit, uinit, xtraj, utraj, emin
  REAL*8 :: val1, val2, dA_num, dA_ana, dA_bar, sig_bar
  REAL*8, ALLOCATABLE :: traj(:), energy(:), relaxtraj(:), relaxenergy(:), du(:), duf(:,:), dub(:,:), dAf(:), dAb(:)
  TYPE(harmonic), TARGET :: ho1, ho2
  TYPE(doublewell), TARGET :: dw1, dw2
  TYPE(hybrid) :: hybridpot

  ! Read input file
  OPEN(UNIT=10,FILE="input.dat",STATUS="OLD") 
  READ(10,*) potflag
  IF (potflag == 1) THEN
    ! potential flag 1: harmonic to harmonic potentials
    READ(10,*) ho1%k, ho1%x0, ho1%u0
    READ(10,*) ho2%k, ho2%x0, ho2%u0
    hybridpot%pot1 => ho1
    hybridpot%pot2 => ho2
  ELSE IF (potflag == 2) THEN
    ! potential flag 2: harmonic to double well potentials
    READ(10,*) ho1%k, ho1%x0, ho1%u0
    READ(10,*) dw2%k, dw2%xa, dw2%xb, dw2%u0
    hybridpot%pot1 => ho1
    hybridpot%pot2 => dw2
  ELSE IF (potflag == 3) THEN
    ! potential flag 3: double well to harmonic potentials 
    READ(10,*) dw1%k, dw1%xa, dw1%xb, dw1%u0
    READ(10,*) ho2%k, ho2%x0, ho2%u0
    hybridpot%pot1 => dw1
    hybridpot%pot2 => ho2
  ELSE IF (potflag == 4) THEN  
    ! potential flag 4: double well to doule well potentials
    READ(10,*) dw1%k, dw1%xa, dw1%xb, dw1%u0
    READ(10,*) dw2%k, dw2%xa, dw2%xb, dw2%u0
    hybridpot%pot1 => dw1
    hybridpot%pot2 => dw2
  END IF
  READ(10,*) nstep, tstep
  READ(10,*) relaxnstep, relaxtstep
  READ(10,*) stepsize, nwindow, nreplica
  READ(10,*) temperature
  CLOSE(10)

  ! Initialization
  CALL SRAND(time())
  kBT = kB * temperature
  beta = 1.d0/kBT
  tdim = nstep/tstep
  relaxtdim = relaxnstep/relaxtstep
  hybridpot%lambda = 0.0
  hybridpot%dlambda = 1.0/nwindow

  ! Allocate arrays
  ALLOCATE(traj(tdim),energy(tdim))
  ALLOCATE(relaxtraj(relaxtdim),relaxenergy(relaxtdim))
  ALLOCATE(dAf(nwindow+1),dAb(nwindow+1))
  ALLOCATE(duf(tdim,nwindow),dub(tdim,nwindow),du(tdim))

  ! Determine the starting position and energy
  IF (potflag < 3) THEN
    xinit = ho1%x0
  ELSE
    xinit = dw1%xa
  END IF
  CALL hybridpot%potential(xinit,uinit)

  ! Double wide sampling
  OPEN(UNIT=10,FILE="FEPorJE.dat",STATUS="UNKNOWN")
  WRITE(10,*), "# window   forward   backward"
  DO i = 1, nwindow+1

    ! update lambda
    hybridpot%lambda = (i-1) * (1.d0/nwindow)

    ! equilibration to relax the system (data not used)
    CALL mcprop(hybridpot,xinit,uinit,relaxnstep,relaxtstep,relaxtraj,relaxenergy)     

    IF (relaxnstep /= 0) THEN
      xinit = relaxtraj(relaxtdim)
    END IF
    !! For sFEP reverse (make BAR crazy, cancel)
    !IF (nwindow == 1 .AND. i==2) THEN
    !  IF (potflag < 3) THEN
    !    xinit = ho2%x0
    !  ELSE
    !    xinit = dw2%xa
    !  END IF
    !END IF
    CALL hybridpot%potential(xinit,uinit)

    ! equilibration to collect data for FEP (relaxnstep != 0) or Jarzynski Equality (relaxnstep=0)
    CALL mcprop(hybridpot,xinit,uinit,nstep,tstep,traj,energy)
    
    ! calculate the du for forward fep
    DO j = 1, tdim
      hybridpot%lambda = hybridpot%lambda + hybridpot%dlambda
      xtraj = traj(j)
      CALL hybridpot%potential(xtraj,utraj)
      du(j) = utraj - energy(j)
      hybridpot%lambda = hybridpot%lambda - hybridpot%dlambda
    ENDDO
    emin = MINVAL(du)
    du = du - emin
    dAf(i) = -kBT * LOG(SUM(EXP(-beta*du))/tdim) + emin
    ! save forward du for BAR
    IF (i < nwindow+1) THEN
      duf(:,i) = du + emin
    END IF

    ! calculate the du for backward fep
    DO j = 1, tdim
      hybridpot%lambda = hybridpot%lambda - hybridpot%dlambda
      xtraj = traj(j)
      CALL hybridpot%potential(xtraj,utraj)
      du(j) = utraj - energy(j)
      hybridpot%lambda = hybridpot%lambda + hybridpot%dlambda
    ENDDO
    emin = MINVAL(du)
    du = du - emin
    dAb(i) = -kBT * LOG(SUM(EXP(-beta*du))/tdim) + emin
    ! save backward du for BAR
    IF (i > 1) THEN
      dub(:,i-1) = du + emin
    END IF
    
    WRITE(10,*) i, dAf(i), dAb(i)

    ! update the xinit and uinit for next window
    xinit = traj(tdim)
    uinit = energy(tdim)

  ENDDO
  WRITE(10,*) "# total ", SUM(dAf(1:nwindow)), SUM(dAb(2:nwindow+1))

  ! Do bar
  CALL bar(duf,dub,dA_bar,sig_bar) 
  WRITE(10,*) "# BAR ", dA_bar, sig_bar

  ! Do numerical dA
  val1 = integrate(hybridpot%pot1,-1.d2,1.d2,1.d-2)
  val2 = integrate(hybridpot%pot2,-1.d2,1.d2,1.d-2)
  dA_num = -kBT * LOG(val2/val1)
  WRITE(10,*) "dA numerical = ", dA_num

  ! Do analytical dA when possible
  IF (potflag == 1) THEN
    val1 = analytical(ho1)
    val2 = analytical(ho2)
    dA_ana = -kBT * LOG(val2/val1)
    WRITE(10,*) "dA analytical = ", dA_ana
  END IF
  CLOSE(10)

END PROGRAM 
