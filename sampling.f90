PROGRAM RAFEP
USE inparas
USE potential1d
USE stats
USE montecarlo
  IMPLICIT NONE
  INTEGER*4 :: i, j, k 
  REAL*8 :: x1, u1, x2, u2, minu, minu2, minu3, x1min, x1max
  REAL*8 :: val1, val2, val3, dA_ana, dA_num, kTlnp, kTlnr, kTln
  REAL*8, ALLOCATABLE :: traj(:), energy(:), relaxtraj(:), relaxenergy(:), relaxeduq(:,:), Ep(:,:)
  TYPE(harmonic), target :: ho1, ho2
  TYPE(doublewell), TARGET :: dw1, dw2
  CLASS(pot1d), POINTER :: gpot1, gpot2    ! polymorphic pointer for potentials

  ! Read input file
  OPEN(UNIT=10,FILE="input.dat",STATUS="OLD")
  READ(10,*) potflag
  IF (potflag == 1) THEN
    ! potential flag 1: harmonic to harmonic potentials
    READ(10,*) ho1%k, ho1%x0, ho1%u0
    READ(10,*) ho2%k, ho2%x0, ho2%u0
    gpot1 => ho1
    gpot2 => ho2
  ELSE IF (potflag == 2) THEN
    ! potential flag 2: harmonic to double well potentials
    READ(10,*) ho1%k, ho1%x0, ho1%u0
    READ(10,*) dw2%k, dw2%xa, dw2%xb, dw2%u0
    gpot1 => ho1
    gpot2 => dw2
  ELSE IF (potflag == 3) THEN
    ! potential flag 3: double well to harmonic potentials 
    READ(10,*) dw1%k, dw1%xa, dw1%xb, dw1%u0
    READ(10,*) ho2%k, ho2%x0, ho2%u0
    gpot1 => dw1
    gpot2 => ho2
  ELSE IF (potflag == 4) THEN
    ! potential flag 4: double well to doule well potentials
    READ(10,*) dw1%k, dw1%xa, dw1%xb, dw1%u0
    READ(10,*) dw2%k, dw2%xa, dw2%xb, dw2%u0
    gpot1 => dw1
    gpot2 => dw2
  END IF
  READ(10,*) nstep, tstep
  READ(10,*) relaxnstep, relaxtstep
  READ(10,*) stepsize, nwindow, nreplica
  READ(10,*) temperature
  CLOSE(10)

  ! Initialization
  CALL SRAND(time())
  kBT = kB * temperature
  beta = 1.d0/kBT
  tdim = nstep/tstep
  relaxtdim = relaxnstep/relaxtstep

  ! Allocate arrays
  ALLOCATE(traj(tdim),energy(tdim),relaxtraj(relaxtdim),relaxenergy(relaxtdim),relaxeduq(nreplica,tdim), Ep(nreplica,tdim))

  OPEN(UNIT=10,FILE="Sampling.dat",STATUS="UNKNOWN")
  ! State R equilibration run (Does not work for double well potential)
  CALL gpot1%getinitpos(0.d0,0.5d0,x1)
  CALL gpot1%potential(x1,u1)
  CALL mcprop(gpot1,x1,u1,nstep,tstep,traj,energy)
  x1min = MINVAL(traj)
  x1max = MAXVAL(traj)

  ! For each microstate of R, perform nreplica relaxation run
  minu = 1.d100
  minu2= 1.d100
  DO i = 1, tdim
    DO j = 1, nreplica
       x2 = traj(i)
       ! potential on perturbed state !
       CALL gpot2%potential(x2,u2)
       CALL mcprop(gpot2,x2,u2,relaxnstep,relaxtstep,relaxtraj,relaxenergy)
       ! store dq
       relaxeduq(j,i) = relaxenergy(relaxtdim) - energy(i)
       Ep(j,i) = relaxenergy(relaxtdim)
       IF (minu  > relaxeduq(j,i)) minu = relaxeduq(j,i)
       IF (minu2 > Ep(j,i)) minu2= Ep(j,i)
    END DO
  END DO
  ! Perform the exponential double layer average
  val1 = 0.d0
  val2 = 0.d0
  DO i = 1, tdim
    DO j = 1, nreplica
      val1 = val1 +  EXP(-beta*(relaxeduq(j,i) - minu))
      val2 = val2 +  EXP(-beta*(Ep(j,i) - minu2))
    END DO
  END DO
  val1 = val1/(nreplica*tdim)
  val1 = -kBT * LOG(val1) + minu  
  val2 = val2/(nreplica*tdim)
  val2 = -kBT * LOG(val2) + minu2
  minu3 = MAXVAL(energy)
  val3 = SUM(EXP(+beta*(energy-minu3)))/tdim
  val3 = -kBT * LOG(val3) - minu3 
  WRITE(10,*) "Sampling -kT*ln<<e^{-beta(u+q)}>_p>_r   = ", val1
  WRITE(10,*) "Sampling -kT*ln<e^{-beta*Ep}>_p         = ", val2
  WRITE(10,*) "Sampling -kT*ln (V_r/Z_r)               = ", -kBT * LOG(x1max-x1min) - helmholtz(gpot1,-1.d2,1.d2,1.d-4)
  WRITE(10,*) "Sampling -kT*ln <e^{+beta*Er}>_r        = ", val3
  WRITE(10,*) "Sampling -kT*ln<V_r/Z_r*e^{-beta*Ep}>_p = ", val2 - kBT * LOG(x1max-x1min) - helmholtz(gpot1,-1.d2,1.d2,1.d-4)
  WRITE(10,*) "Sampling V_r                            = ", x1max - x1min
  CLOSE(10)


  OPEN(UNIT=10,FILE="Numerical.dat",STATUS="UNKNOWN")
  ! Do numerical dA
  val1 = helmholtz(gpot1,-1.d2,1.d2,1.d-4)
  val2 = helmholtz(gpot2,-1.d2,1.d2,1.d-4)
  dA_num = val2 - val1
  WRITE(10,*) "A_p = ", val2
  WRITE(10,*) "A_r = ", val1
  WRITE(10,*) "dA numerical = ", dA_num
  
  ! Do numerical <<e^{-beta(u+q)}>>
  CALL integrate_uq(gpot1,gpot2,-1.d2,1.d2,1.d-4,kTlnp,kTlnr,kTln)
  WRITE(10,*) "-kT*ln<e^{-beta*Ep}>_p = ", kTlnp
  WRITE(10,*) "-kT*ln<e^{+beta*Er}>_r = ", kTlnr
  WRITE(10,*) "-kT*ln<<e^{-beta(u+q)}>_p>_r = ", kTln

! ! Do analytical dA when possible
! IF (potflag == 1) THEN
!   val1 = analytical(ho1)
!   val2 = analytical(ho2)
!   dA_ana = -kBT * LOG(val2/val1)
!   WRITE(10,*) "dA analytical = ", dA_ana
! END IF
  CLOSE(10)


END PROGRAM 
