! module for the 1d potentials
module pot1d

   implicit none

   ! define abstract type for potential functions
   ! only common data is a description
   type,abstract :: pot1d_t
      character(len=50) :: desc
   contains
      procedure(pot1d_iface),deferred :: pot
   end type pot1d_t

   ! pointer type for the above, needed to have arrays of pot1d_t
   type :: pot1d_tp
      class(pot1d_t),pointer :: p => null()
   end type pot1d_tp

   ! interface of the potential function evaluation
   ! must be implemented by each concrete type
   abstract interface
      subroutine pot1d_iface(self,x,v)
         import :: pot1d_t
         class(pot1d_t),intent(in) :: self
         real*8,intent(in)  :: x
         real*8,intent(out) :: v
      end subroutine pot1d_iface
   end interface


   ! concrete type for single-well potential
   type,extends(pot1d_t) :: well_t
      real*8 :: a
      real*8 :: x0
   contains
      procedure :: pot => pot1d_well
   end type well_t

   ! interface for constructor
   interface well_t
      procedure :: new_well
   end interface well_t


   ! concrete type for double-well potential
   type,extends(pot1d_t) :: doublewell_t
      real*8 :: a
      real*8 :: x0
      real*8 :: x1
   contains
      procedure :: pot => pot1d_doublewell
   end type doublewell_t

   ! interface for constructor
   interface doublewell_t
      procedure :: new_doublewell
   end interface doublewell_t


   ! concrete type for hybrid potential (linear combo of two other potentials)
   type,extends(pot1d_t) :: hybrid_t
      class(pot1d_t),pointer :: pot1
      class(pot1d_t),pointer :: pot2
      real*8                 :: lambda
   contains
      procedure :: pot => pot1d_hybrid
   end type hybrid_t


contains

   ! implementation of constructor for well_t
   type(well_t) function new_well(a,x0)
      real*8,intent(in) :: a,x0
      new_well%a = a
      new_well%x0 = x0
      new_well%desc = "single-well"
   end function new_well

   ! implementation of potential evaluation for well_t
   subroutine pot1d_well(self,x,v)
      class(well_t),intent(in) :: self
      real*8,intent(in)  :: x
      real*8,intent(out) :: v
      v = self%a * (x - self%x0)**2
   end subroutine pot1d_well


   ! implementation of constructor for doublewell_t
   type(doublewell_t) function new_doublewell(a,x0,x1)
      real*8,intent(in) :: a,x0,x1
      new_doublewell%a = a
      new_doublewell%x0 = x0
      new_doublewell%x1 = x1
      new_doublewell%desc = "double-well"
   end function new_doublewell

   ! implementation of potential evaluation for doublewell_t
   ! TODO: this is not actually a double well
   subroutine pot1d_doublewell(self,x,v)
      class(doublewell_t),intent(in) :: self
      real*8,intent(in)  :: x
      real*8,intent(out) :: v
      v = self%a * ( (x - self%x0)**2 + (x - self%x1)**2 )
   end subroutine pot1d_doublewell


   ! implementation of potential evaluation for hybrid_t
   subroutine pot1d_hybrid(self,x,v)
      class(hybrid_t),intent(in) :: self
      real*8,intent(in)  :: x
      real*8,intent(out) :: v
      real*8 :: v1,v2
      call self%pot1%pot(x,v1)
      call self%pot2%pot(x,v2)
      v = self%lambda*v1 + (1.d0-self%lambda)*v2
   end subroutine pot1d_hybrid



end module pot1d



! module for testing the above
module test

   use pot1d

contains

   ! evaluates the given 1D potential on a bunch of points
   ! print point and potential at that point
   subroutine test_pot(pot,xs)
      class(pot1d_t),intent(in) :: pot
      real*8,intent(in) :: xs(:)
      real*8 :: x,v
      integer :: i
      do i=1,size(xs)
         x = xs(i)
         call pot%pot(x,v)
         print *, x, v
      end do
   end subroutine test_pot

end module test



! main program, driving the test
program main

   use pot1d
   use test

   integer,parameter :: n = 11    ! number of points
   real*8,parameter :: L  = 2.d0  ! will be equi-spaced in [-L/2, +L/2]

   real*8                    :: xs(n)   ! the points
   type(well_t),target       :: well    ! holds a single-well potential
   type(doublewell_t),target :: dwell   ! holds a double-well potential
   type(hybrid_t),target     :: hybrid
   ! class(pot1d_t) :: pots(2) !!! NOT ALLOWED !!!
   type(pot1d_tp)            :: pots(3) ! polymorphic array holding all potentials
   class(pot1d_t),pointer    :: pot     ! polymorphic pointer holding one of the potentials
   integer                   :: i

   ! set up the points
   do i=1,n
      xs(i) = (i-1) * L/(n-1) - L/2
   end do

   ! create the potentials
   well = new_well(1.d0, 0.d0)
   dwell = new_doublewell(0.5d0, -1.d0, 1.d0)
   ! store them in the polymorphic array
   pots(1)%p => well
   pots(2)%p => dwell
   ! create hybrid potential
   hybrid%pot1 => well
   hybrid%pot2 => dwell
   hybrid%lambda = 0.4
   hybrid%desc = "hybrid"
   pots(3)%p => hybrid

   ! iterate of the potentials
   do i=1,3
      ! get the current potential
      pot => pots(i)%p
      ! print its description
      print *, '# ', pot%desc
      ! run the test code on it
      call test_pot(pot,xs)
      print *
   end do

end program main

