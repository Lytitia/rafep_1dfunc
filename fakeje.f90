PROGRAM FAKEJE
USE inparas
USE potential1d
USE montecarlo
USE stats
  IMPLICIT NONE
  INTEGER*4 :: i, j, k
  REAL*8 :: x1, x2, u1, u2, emin, dA
  REAL*8 :: val1, val2, dA_num, dA_ana
  REAL*8, ALLOCATABLE :: traj(:,:), energy(:,:), du(:,:), work(:)
  REAL*8, ALLOCATABLE :: relaxtraj(:), relaxenergy(:), dQ(:)
  TYPE(harmonic), TARGET :: ho1, ho2
  TYPE(doublewell), TARGET :: dw1, dw2
  CLASS(pot1d), POINTER :: gpot1, gpot2

  ! Read input file
  OPEN(UNIT=10,FILE="input.dat",STATUS="OLD") 
  READ(10,*) potflag
  IF (potflag == 1) THEN
    ! potential flag 1: harmonic to harmonic potentials
    READ(10,*) ho1%k, ho1%x0, ho1%u0
    READ(10,*) ho2%k, ho2%x0, ho2%u0
    gpot1 => ho1
    gpot2 => ho2
  ELSE IF (potflag == 2) THEN
    ! potential flag 2: harmonic to double well potentials
    READ(10,*) ho1%k, ho1%x0, ho1%u0
    READ(10,*) dw2%k, dw2%xa, dw2%xb, dw2%u0
    gpot1 => ho1
    gpot2 => dw2
  ELSE IF (potflag == 3) THEN
    ! potential flag 3: double well to harmonic potentials 
    READ(10,*) dw1%k, dw1%xa, dw1%xb, dw1%u0
    READ(10,*) ho2%k, ho2%x0, ho2%u0
    gpot1 => dw1
    gpot2 => ho2
  ELSE IF (potflag == 4) THEN  
    ! potential flag 4: double well to doule well potentials
    READ(10,*) dw1%k, dw1%xa, dw1%xb, dw1%u0
    READ(10,*) dw2%k, dw2%xa, dw2%xb, dw2%u0
    gpot1 => dw1
    gpot2 => dw2
  END IF
  READ(10,*) nstep, tstep
  READ(10,*) relaxnstep, relaxtstep
  READ(10,*) stepsize, nwindow, nreplica
  READ(10,*) temperature
  CLOSE(10)

  ! Initialization
  CALL SRAND(time())
  kBT = kB * temperature
  beta = 1.d0/kBT
  tdim = nstep/tstep
  relaxtdim = relaxnstep/relaxtstep

  ! Allocate arrays
  ALLOCATE(traj(tdim,nwindow),energy(tdim,nwindow),du(tdim,nwindow))
  ALLOCATE(work(tdim*nwindow))
  ALLOCATE(relaxtraj(relaxtdim),relaxenergy(relaxtdim),dQ(nreplica))

  ! Chop the run into multiple short runs (nwindow)
  DO j = 1, nwindow

    ! Perform MC equilibration run with initial position given according to
    ! Boltzmann weight between minimums
    CALL gpot1%getinitpos(DBLE(RAND()),0.5d0,x1)
    CALL gpot1%potential(x1,u1)
    CALL mcprop(gpot1,x1,u1,nstep,tstep,traj(:,j),energy(:,j))

  END DO
  
  OPEN(UNIT=10,FILE="FakeJE.dat",STATUS="UNKNOWN")
  ! Single step FEP
  DO j = 1, nwindow
    DO i = 1, tdim
      x2 = traj(i,j)
      CALL gpot2%potential(x2,u2)
      du(i,j) = u2 - energy(i,j)
    END DO
  END DO
  work = RESHAPE(du,(/tdim*nwindow/))
  emin = MINVAL(work)
  work = work - emin
  dA = -kBT * LOG(SUM(EXP(-beta*work))/tdim) + emin

  ! Calculate the weighted relaxation energy (Fake Jarzynski Equality)
  DO i = 1, nreplica

    DO k = 1, nwindow
      DO j = 1, tdim
        x2 = traj(j,k)
        CALL gpot2%potential(x2,u2)
        CALL mcprop(gpot2,x2,u2,relaxnstep,relaxtstep,relaxtraj,relaxenergy)
        du(j,k) = u2 - relaxenergy(relaxtdim)
      END DO
    END DO
    work = RESHAPE(du,(/tdim*nwindow/))
    emin = MINVAL(work)
    work = work - emin
    dQ(i) = -kBT * LOG(SUM(EXP(-beta*work))/tdim) + emin

  END DO
  
  WRITE(10,*) "Single step FEP = ", dA
  WRITE(10,*) "Average dQ = ", mean(dQ), std(dQ)
  WRITE(10,*) "dA - dQ = ", dA-mean(dQ), std(dQ)

  ! Do numerical dA
  val1 = integrate(gpot1,-1.d2,1.d2,1.d-2)
  val2 = integrate(gpot2,-1.d2,1.d2,1.d-2)
  dA_num = -kBT * LOG(val2/val1)
  WRITE(10,*) "dA numerical = ", dA_num

  ! Do analytical dA when possible
  IF (potflag == 1) THEN
    val1 = analytical(ho1)
    val2 = analytical(ho2)
    dA_ana = -kBT * LOG(val2/val1)
    WRITE(10,*) "dA numerical = ", dA_num
  END IF
  CLOSE(10)

END PROGRAM 
