PROGRAM NUMERICAL
USE inparas
USE potential1d
USE montecarlo
  INTEGER*4 :: i, j, k 
  REAL*8 :: val1, val2, dA_num, kTlnp, kTlnr, kTln
  TYPE(harmonic), target :: ho1, ho2
  TYPE(doublewell), TARGET :: dw1, dw2
  CLASS(pot1d), POINTER :: gpot1, gpot2    ! polymorphic pointer for potentials

  ! Read input file
  OPEN(UNIT=10,FILE="input.dat",STATUS="OLD")
  READ(10,*) potflag
  IF (potflag == 1) THEN
    ! potential flag 1: harmonic to harmonic potentials
    READ(10,*) ho1%k, ho1%x0, ho1%u0
    READ(10,*) ho2%k, ho2%x0, ho2%u0
    gpot1 => ho1
    gpot2 => ho2
  ELSE IF (potflag == 2) THEN
    ! potential flag 2: harmonic to double well potentials
    READ(10,*) ho1%k, ho1%x0, ho1%u0
    READ(10,*) dw2%k, dw2%xa, dw2%xb, dw2%u0
    gpot1 => ho1
    gpot2 => dw2
  ELSE IF (potflag == 3) THEN
    ! potential flag 3: double well to harmonic potentials 
    READ(10,*) dw1%k, dw1%xa, dw1%xb, dw1%u0
    READ(10,*) ho2%k, ho2%x0, ho2%u0
    gpot1 => dw1
    gpot2 => ho2
  ELSE IF (potflag == 4) THEN
    ! potential flag 4: double well to doule well potentials
    READ(10,*) dw1%k, dw1%xa, dw1%xb, dw1%u0
    READ(10,*) dw2%k, dw2%xa, dw2%xb, dw2%u0
    gpot1 => dw1
    gpot2 => dw2
  END IF
  READ(10,*) nstep, tstep
  READ(10,*) relaxnstep, relaxtstep
  READ(10,*) stepsize, nwindow, nreplica
  READ(10,*) temperature
  CLOSE(10)

  ! Initialization
  kBT = kB * temperature
  beta = 1.d0/kBT
  tdim = nstep/tstep
  relaxtdim = relaxnstep/relaxtstep

  OPEN(UNIT=10,FILE="Numerical.dat",STATUS="UNKNOWN")
  ! Do numerical dA
  val1 = helmholtz(gpot1,-1.d3,1.d3,1.d-4)
  val2 = helmholtz(gpot2,-1.d3,1.d3,1.d-4)
  dA_num = val2 - val1
  WRITE(10,*) "A_p = ", val2
  WRITE(10,*) "A_r = ", val1
  WRITE(10,*) "dA numerical = ", dA_num
  
  ! Do numerical <<e^{-beta(u+q)}>>
  CALL integrate_uq(gpot1,gpot2,-1.d3,1.d3,1.d-4,kTlnp,kTlnr,kTln)
  WRITE(10,*) "-kT*ln<e^{-beta*Ep}>_p = ", kTlnp
  WRITE(10,*) "-kT*ln<e^{+beta*Er}>_r = ", kTlnr
  WRITE(10,*) "-kT*ln<<e^{-beta(u+q)}>_p>_r = ", kTln

  CLOSE(10)


END PROGRAM 
