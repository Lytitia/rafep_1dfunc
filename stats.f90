! Often used statisitcal function.  
! Letitia: 2019.06.13
MODULE stats
  IMPLICIT NONE 
  CONTAINS

  FUNCTION mean(Arr) RESULT(avg)
    INTEGER*4 :: ndim
    REAL*8, INTENT(IN) :: Arr(:)
    REAL*8 :: avg
    ndim = SIZE(Arr)
    avg = SUM(Arr)*(1.d0/ndim)
  END FUNCTION
  
  FUNCTION var(Arr) RESULT(variance)
    INTEGER*4 :: ndim
    REAL*8, INTENT(IN) :: Arr(:)
    REAL*8 :: variance, avg
    avg = mean(Arr)
    ndim = size(Arr)
    variance = DOT_PRODUCT(Arr-avg, Arr-avg) * (1.0/ndim)
  END FUNCTION
  
  FUNCTION std(Arr) RESULT(deviation)
    REAL*8, INTENT(IN) :: Arr(:) 
    REAL*8 :: deviation
    deviation = SQRT(var(Arr))
  END FUNCTION

END MODULE
