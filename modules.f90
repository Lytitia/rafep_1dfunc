MODULE inparas
! Input parameters 
  IMPLICIT NONE
  INTEGER*4, SAVE :: nreplica, nstep, tstep, tdim
  INTEGER*4, SAVE :: relaxnstep, relaxtstep, relaxtdim
  INTEGER*4, SAVE :: nwindow
  INTEGER*4, SAVE :: potflag
  REAL*8, SAVE :: temperature, kBT, beta, stepsize
  REAL*8, SAVE :: lambda = 0.d0
  REAL*8, PARAMETER :: kB = 0.0019872041
  REAL*8, PARAMETER :: PI = 3.1415926
  !CHARACTER(LEN=20) :: init_method
END MODULE


MODULE potential1d
! 1d potentials
  IMPLICIT NONE

    ! define abstract type for potential functions (requires interface setup for deferred function)
    TYPE,ABSTRACT :: pot1d
    CONTAINS
      PROCEDURE(potential_iface), DEFERRED :: potential
      PROCEDURE(getinitpos_iface), DEFERRED :: getinitpos
    END TYPE
  
    ! interface setup for deferred function
    ABSTRACT INTERFACE

      SUBROUTINE potential_iface(self,x,u)
        IMPORT :: pot1d
        CLASS(pot1d),INTENT(IN) :: self
        REAL*8,INTENT(IN)  :: x
        REAL*8,INTENT(OUT) :: u
      END SUBROUTINE

      SUBROUTINE getinitpos_iface(self,val1,val2,xinit)
        IMPORT :: pot1d
        CLASS(pot1d),INTENT(IN) :: self
        REAL*8,INTENT(IN) :: val1, val2
        REAL*8,INTENT(OUT) :: xinit
      END SUBROUTINE

    END INTERFACE

    ! define pointer type for the above to allow pointer/array of class(pot1d)
    TYPE :: pot1d_pointer
      CLASS(pot1d),POINTER :: p => null()
    END TYPE pot1d_pointer


    ! define the concrete type for harmonic oscillator
    TYPE,EXTENDS(pot1d) :: harmonic
      REAL*8 :: k
      REAL*8 :: x0
      REAL*8 :: u0
    CONTAINS
      PROCEDURE :: potential => harmonic_potential
      PROCEDURE :: getinitpos => harmonic_getinitpos
    END TYPE

    ! define the concrete type for double well potential
    TYPE, EXTENDS(pot1d) :: doublewell
      REAL*8 :: k
      REAL*8 :: xa, xb
      REAL*8 :: u0
    CONTAINS
      PROCEDURE :: potential => doublewell_potential
      PROCEDURE :: getinitpos => doublewell_getinitpos
    END TYPE

    ! define the concrete type as a hybrid of two potential
    TYPE, EXTENDS(pot1d) :: hybrid
      CLASS(pot1d), POINTER :: pot1
      CLASS(pot1d), POINTER :: pot2
      REAL*8 :: lambda, dlambda
    CONTAINS
      PROCEDURE :: potential => hybrid_potential
      PROCEDURE :: getinitpos => hybrid_getinitpos
    END TYPE
           

  CONTAINS

    ! define the potential function for the concrete type harmonic
    SUBROUTINE harmonic_potential(self,x,u)
      CLASS(harmonic), INTENT(IN) :: self
      REAL*8, INTENT(IN) :: x
      REAL*8, INTENT(OUT) :: u
      u = 0.5d0 * self%k * (x-self%x0)**2 + self%u0
    END SUBROUTINE

    ! define the potential function for the concrete type doublewell
    SUBROUTINE doublewell_potential(self,x,u)
      CLASS(doublewell), INTENT(IN) :: self
      REAL*8, INTENT(IN) :: x
      REAL*8, INTENT(OUT) :: u
      u = self%k * (x-self%xa)**2 * (x-self%xb)**2 + self%u0
    END SUBROUTINE

    ! implementation of potential evaluation for hybrid_t
    SUBROUTINE hybrid_potential(self,x,u)
       CLASS(hybrid),INTENT(IN) :: self
       REAL*8,INTENT(IN)  :: x
       REAL*8,INTENT(OUT) :: u
       REAL*8 :: u1,u2
       call self%pot1%potential(x,u1)
       call self%pot2%potential(x,u2)
       u = u1 + self%lambda*(u2-u1)
    END SUBROUTINE

    ! define the getinitpos function for the concrete type harmonic
    SUBROUTINE harmonic_getinitpos(self,val1,val2,xinit)
      CLASS(harmonic), INTENT(IN) :: self
      REAL*8, INTENT(IN) :: val1, val2
      REAL*8, INTENT(OUT) :: xinit
      xinit = self%x0
    END SUBROUTINE

    ! define the getinitpos function for the concrete type harmonic
    SUBROUTINE doublewell_getinitpos(self,val1,val2,xinit)
      CLASS(doublewell), INTENT(IN) :: self
      REAL*8, INTENT(IN) :: val1, val2
      REAL*8, INTENT(OUT) :: xinit
      IF (val1 <= val2) THEN
        xinit = self%xa
      ELSE
        xinit = self%xb
      END IF
    END SUBROUTINE

    ! define a dummy getinitpos function for the concrete type hybrid
    SUBROUTINE hybrid_getinitpos(self,val1,val2,xinit)
      CLASS(hybrid), INTENT(IN) :: self
      REAL*8, INTENT(IN) :: val1, val2
      REAL*8, INTENT(OUT) :: xinit
      xinit = 0.d0
    END SUBROUTINE


END MODULE

MODULE montecarlo
! Monte Carlo propagation
  IMPLICIT NONE

  CONTAINS
    SUBROUTINE mcprop(pot,xnow,enow,mcstep,mcoutstep,traj,energy)
      USE inparas
      USE potential1d
      IMPLICIT NONE
      CLASS(pot1d) :: pot
      INTEGER*4 :: i, tindex, mcstep, mcoutstep
      REAL*8 :: prob, accept
      REAL*8 :: xnew, xold, xnow
      REAL*8 :: enew, eold, enow
      REAL*8 :: traj(:), energy(:)
      
      xold = xnow
      eold = enow
      accept = 0.d0
      tindex = 1

      DO i = 1, mcstep

        IF (MOD(i,mcoutstep) == 0) THEN
           traj(tindex) = xold
           energy(tindex) = eold
           tindex = tindex + 1
        ENDIF

        xnew = xold + (RAND() - 0.5) * stepsize * 2.d0
        CALL pot%potential(xnew,enew)

        IF (enew < eold) THEN
          xold = xnew
          eold = enew 
          accept = accept + 1.d0
          CYCLE
        ENDIF

        prob = exp(-beta*(enew-eold))
        IF (RAND() <= prob) THEN
          xold = xnew
          eold = enew 
          accept = accept + 1.d0
          CYCLE
        ELSE
          CYCLE
        ENDIF

      ENDDO

     !accept = accept / mcstep
     !PRINT *, accept

    END SUBROUTINE

    FUNCTION integrate(pot,xinit,xend,xstep) RESULT(val)
    ! calucalte the partition function numerically
      USE potential1d
      USE inparas
      IMPLICIT NONE
      CLASS(pot1d) :: pot
      INTEGER*4 :: i, xdim
      REAL*8 :: xinit, xstep, xend, x, val, u
      
      xdim = NINT((xend-xinit)/xstep) + 1
      val = 0.d0
      DO i = 1,xdim
        x = (i-1)*xstep + xinit
        CALL pot%potential(x,u)
        val = val + EXP(-beta*u)
      ENDDO
      CALL pot%potential(xinit,u)
      val = val - 0.5d0 * EXP(-beta*u)
      CALL pot%potential(xend,u)
      val = val - 0.5d0 * EXP(-beta*u)

    END FUNCTION

    FUNCTION integrate_uq(potr,potp,xinit,xend,xstep) RESULT(val)
    ! calculate the intgral over < < e^{-beta(u+q)} >_P >_R numerically
      USE potential1d
      USE inparas
      IMPLICIT NONE
      CLASS(pot1d) :: potr, potp
      INTEGER*4 :: i, xdim
      REAL*8 :: xinit, xstep, xend, x, val, Zr, Zp, Vr, u
      
      Zr=integrate(potr,xinit,xend,xstep)
      Zp=integrate(potp,xinit,xend,xstep)
      Vr=(xend-xinit)
      
      xdim = NINT((xend-xinit)/xstep) + 1
      val = 0.d0
      DO i = 1, xdim
        x = (i-1)*xstep + xinit
        CALL potp%potential(x,u)
        val = val + EXP(-2*beta*u)
      ENDDO
      CALL potp%potential(xinit,u)
      val = val - 0.5d0 * EXP(-2*beta*u)
      CALL potp%potential(xend,u)
      val = val - 0.5d0 * EXP(-2*beta*u)
      
      val = val * Vr / (Zr * Zp)

    END FUNCTION


    FUNCTION analytical(hopar) RESULT(val)
    ! calculate the harmonic oscillator's partition function analytically
      USE potential1d
      USE inparas
      IMPLICIT NONE
      TYPE(harmonic) :: hopar
      REAL*8 :: val
      val = EXP(-beta*hopar%u0) * SQRT(2 * PI / (beta* hopar%k))
    END FUNCTION

    FUNCTION volume(traj) RESULT(val)
    ! calculate the histogram for 1d trajectory (needed due to doublewell potential)
       USE inparas
       IMPLICIT NONE
       REAL*8 :: val, binsize, traj(:)
       REAL*8, ALLOCATABLE :: hist(:)
       INTEGER*4 :: i, ndim, binid

       !binsize = stepsize
       binsize = 0.02
       ndim = NINT(200.0/binsize) + 1
       ALLOCATE(hist(ndim))

       DO i = 1, ndim
         hist(i) = 0
       END DO       

       DO i = 1, SIZE(traj)
         binid = NINT((traj(i)+100)/binsize) + 1
         hist(binid) = 1
       END DO

       val = SUM(hist) * binsize
    END FUNCTION

    SUBROUTINE bar(duf,dub,val1,val2)
    ! calculate bar 
      USE inparas
      USE stats
      IMPLICIT NONE
      INTEGER*4 :: i, j, ind, ndim
      REAL*8 :: val1, val2
      REAL*8 :: C, FDfavg, FDbavg, FDfvar, FDbvar, dAprev
      REAL*8 :: duf(:,:), dub(:,:)
      REAL*8, ALLOCATABLE :: FDf(:), FDb(:), dA(:), sigma(:)
      LOGICAL flag
      ALLOCATE(FDf(tdim),FDb(tdim),dA(nwindow),sigma(nwindow))    
  
      ndim = SIZE(duf)
      IF (ndim /= nwindow*tdim) THEN
        PRINT *, "Error! ndim != nwindow * tdim"
        STOP
      END IF
      
      ! scale energy with beta
      duf = duf * beta
      dub = dub * beta
     
      ! loop over each window
      DO i=1,nwindow
        ! initial guess
        dA(i) = 1.0
        C = dA(i)
        dAprev = dA(i)
        flag = .TRUE.
        DO WHILE (flag) 
          ! forward:  f(duf - C/beta)
          ! backward: f(dub + C/beta)
          DO j=1,tdim
            FDf(j) = 1.d0 / (1.d0 + EXP(duf(j,i)-C))
            FDb(j) = 1.d0 / (1.d0 + EXP(dub(j,i)+C))
          END DO
          FDfavg = mean(FDf)
          FDbavg = mean(FDb)
          FDfvar = var(FDf)
          FDbvar = var(FDb)
          ! evaluate the standard error
          sigma(i) = SQRT(FDfvar/tdim/(FDfavg**2)+FDbvar/tdim/(FDbavg**2))
          ! update C, dA, dAprev
          dA(i) = LOG(FDbavg/FDfavg) + C
          C = dA(i)
          IF (ABS(dA(i)-dAprev) < 1.d-6) THEN
            flag = .FALSE.
          END IF
          dAprev = dA(i)
        END DO
      END DO
      
      ! accumulate over all windows
      val1 = SUM(dA)*kBT 
      val2 = SUM(sigma)*kBT

    END SUBROUTINE

END MODULE


