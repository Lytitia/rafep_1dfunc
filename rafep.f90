PROGRAM RAFEP
USE inparas
USE potential1d
USE stats
USE montecarlo
  INTEGER*4 :: i, j, k 
  REAL*8 :: x1, u1, x2, u2
  REAL*8 :: emax, e1, e2, v1, v2
  REAL*8 :: val1, val2, dA_ana, dA_num
  REAL*8, ALLOCATABLE :: traj1(:,:), energy1(:,:), traj2(:,:), energy2(:,:), energy(:), work(:), dE(:), dV(:), dA(:), work2(:)
  REAL*8, ALLOCATABLE :: relaxtraj(:), relaxenergy(:)
  TYPE(harmonic), target :: ho1, ho2
  TYPE(doublewell), TARGET :: dw1, dw2
  CLASS(pot1d), POINTER :: gpot1, gpot2    ! polymorphic pointer for potentials

  ! Read input file
  OPEN(UNIT=10,FILE="input.dat",STATUS="OLD")
  READ(10,*) potflag
  IF (potflag == 1) THEN
    ! potential flag 1: harmonic to harmonic potentials
    READ(10,*) ho1%k, ho1%x0, ho1%u0
    READ(10,*) ho2%k, ho2%x0, ho2%u0
    gpot1 => ho1
    gpot2 => ho2
  ELSE IF (potflag == 2) THEN
    ! potential flag 2: harmonic to double well potentials
    READ(10,*) ho1%k, ho1%x0, ho1%u0
    READ(10,*) dw2%k, dw2%xa, dw2%xb, dw2%u0
    gpot1 => ho1
    gpot2 => dw2
  ELSE IF (potflag == 3) THEN
    ! potential flag 3: double well to harmonic potentials 
    READ(10,*) dw1%k, dw1%xa, dw1%xb, dw1%u0
    READ(10,*) ho2%k, ho2%x0, ho2%u0
    gpot1 => dw1
    gpot2 => ho2
  ELSE IF (potflag == 4) THEN
    ! potential flag 4: double well to doule well potentials
    READ(10,*) dw1%k, dw1%xa, dw1%xb, dw1%u0
    READ(10,*) dw2%k, dw2%xa, dw2%xb, dw2%u0
    gpot1 => dw1
    gpot2 => dw2
  END IF
  READ(10,*) nstep, tstep
  READ(10,*) relaxnstep, relaxtstep
  READ(10,*) stepsize, nwindow, nreplica
  READ(10,*) temperature
  CLOSE(10)

  ! Initialization
  CALL SRAND(time())
  kBT = kB * temperature
  beta = 1.d0/kBT
  tdim = nstep/tstep
  relaxtdim = relaxnstep/relaxtstep

  ! Allocate arrays
  ALLOCATE(traj1(tdim,nwindow),energy1(tdim,nwindow),traj2(tdim,nwindow),energy2(tdim,nwindow),work(tdim*nwindow),work2(tdim))
  ALLOCATE(relaxtraj(relaxtdim),relaxenergy(relaxtdim))
  ALLOCATE(dA(nreplica),dE(nreplica),dV(nreplica))

! OPEN(UNIT=10,FILE="RAFEP_equ.dat",STATUS="UNKNOWN")
! WRITE(10,*) "#  id  dE  dV  dA"
! ! RAFEP through equilibration run
! DO i = 1, nreplica

!   ! Use multiple run (nwindow)
!   DO j = 1, nwindow

!     ! Perform MC equilibration run with initial position given according to Boltzmann weight between minimums
!     !CALL gpot1%getinitpos(DBLE(RAND()),0.5d0,x1)
!     !CALL gpot2%getinitpos(DBLE(RAND()),0.5d0,x2)
!     IF (MOD(j,2) == 0) THEN
!       CALL gpot1%getinitpos(0.d0,0.5d0,x1)
!       CALL gpot2%getinitpos(0.d0,0.5d0,x2)
!     ELSE
!       CALL gpot1%getinitpos(1.d0,0.5d0,x1)
!       CALL gpot2%getinitpos(1.d0,0.5d0,x2)
!     END IF  
!     print *, x1, x2
!     CALL gpot1%potential(x1,u1)
!     CALL gpot2%potential(x2,u2)
!     CALL mcprop(gpot1,x1,u1,nstep,tstep,traj1(:,j),energy1(:,j))
!     CALL mcprop(gpot2,x2,u2,nstep,tstep,traj2(:,j),energy2(:,j))

!   END DO

!   ! RAFEP calculations
!   work = RESHAPE(energy1,(/tdim*nwindow/))
!   emax = MAXVAL(work)
!   e1 = kBT * LOG(SUM(EXP(beta*(work - emax)))) + emax
!   work = RESHAPE(traj1,(/tdim*nwindow/))
!   v1 = kBT * LOG(volume(work))
!   print *, "v1 = ", MINVAL(work), MAXVAL(work), volume(work)
!   work = RESHAPE(energy2,(/tdim*nwindow/))
!   emax = MAXVAL(work)
!   e2 = kBT * LOG(SUM(EXP(beta*(work - emax)))) + emax
!   work = RESHAPE(traj2,(/tdim*nwindow/))
!   v2 = kBT * LOG(volume(work))
!   print *, "v2 = ", MINVAL(work), MAXVAL(work), volume(work)

!   dE(i) = (e2 - e1)
!   dV(i) = - (v2 - v1)
!   dA(i) = dE(i) + dV(i)
!   WRITE(10,*) i, dE(i), dV(i), dA(i)

! ENDDO
! WRITE(10,*), "# dE avg and std: ", mean(dE), std(dE)
! WRITE(10,*), "# dV avg and std: ", mean(dV), std(dV)
! WRITE(10,*), "# dA avg and std: ", mean(dA), std(dA)
! CLOSE(10)

! OPEN(UNIT=10,FILE="RAFEP_relax.dat",STATUS="UNKNOWN")
! WRITE(10,*) "#  id  dE  dV  dA"
! ! RAFEP through relaxation run
! DO i=1, nreplica
!   
!   ! Chop the run into multiple short runs (nwindow)
!   DO j = 1, nwindow

!     CALL gpot1%getinitpos(DBLE(RAND()),0.5d0,x1)
!     CALL gpot1%potential(x1,u1)
!     CALL mcprop(gpot1,x1,u1,nstep,tstep,traj1(:,j),energy1(:,j))

!   END DO

!   DO j = 1, nwindow
!     DO k = 1, tdim
!       x2 = traj1(k,j)
!       CALL gpot2%potential(x2,u2)
!       CALL mcprop(gpot2,x2,u2,relaxnstep,relaxtstep,relaxtraj,relaxenergy)
!       traj2(k,j) = relaxtraj(relaxtdim)
!       energy2(k,j) = relaxenergy(relaxtdim)
!     END DO
!   ENDDO
!
!   ! RAFEP calculations
!   work = RESHAPE(energy1,(/tdim*nwindow/))
!   emax = MAXVAL(work)
!   e1 = kBT * LOG(SUM(EXP(beta*(work - emax)))) + emax
!   work = RESHAPE(traj1,(/tdim*nwindow/))
!   v1 = kBT * LOG(volume(work))
!   work = RESHAPE(energy2,(/tdim*nwindow/))
!   emax = MAXVAL(work)
!   e2 = kBT * LOG(SUM(EXP(beta*(work - emax)))) + emax
!   work = RESHAPE(traj2,(/tdim*nwindow/))
!   v2 = kBT * LOG(volume(work))

!  !emax = MAXVAL(energy1)
!  !e1 = kBT * LOG(SUM(EXP(beta*(energy1 - emax)))) + emax
!  !v1 = kBT * LOG(volume(traj1))
!  !emax = MAXVAL(energy2)
!  !e2 = kBT * LOG(SUM(EXP(beta*(energy2 - emax)))) + emax
!  !v2 = kBT * LOG(volume(traj2))

!   dE(i) = (e2 - e1)
!   dV(i) = - (v2 - v1)
!   dA(i) = dE(i) + dV(i)
!   WRITE(10,*) i, dE(i), dV(i), dA(i)

! ENDDO
! WRITE(10,*), "# dE avg and std: ", mean(dE), std(dE)
! WRITE(10,*), "# dV avg and std: ", mean(dV), std(dV)
! WRITE(10,*), "# dA avg and std: ", mean(dA), std(dA)

  OPEN(UNIT=10,FILE="Numerical.dat",STATUS="UNKNOWN")
  ! Do numerical dA
  val1 = integrate(gpot1,-1.d2,1.d2,1.d-2)
  val2 = integrate(gpot2,-1.d2,1.d2,1.d-2)
  dA_num = -kBT * LOG(val2/val1)
  WRITE(10,*) "dA numerical = ", dA_num
  
  ! Do numerical <<e^{-beta(u+q)}>>
  val1 = integrate_uq(gpot1,gpot2,-1.d2,1.d2,1.d-2)
  WRITE(10,*) "<<e^{-beta(u+q)}>> = ", val1

  ! Do analytical dA when possible
  IF (potflag == 1) THEN
    val1 = analytical(ho1)
    val2 = analytical(ho2)
    dA_ana = -kBT * LOG(val2/val1)
    WRITE(10,*) "dA analytical = ", dA_ana
  END IF
  CLOSE(10)


END PROGRAM 
